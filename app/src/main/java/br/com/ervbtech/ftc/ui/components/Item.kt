package br.com.ervbtech.ftc.ui.components

import androidx.annotation.DrawableRes

data class Item(
        val title: String,
        @DrawableRes val icon: Int
)