package br.com.ervbtech.ftc.data.financialtrack;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

import br.com.ervbtech.ftc.data.category.Category;
import br.com.ervbtech.ftc.data.category.CategoryEnum;
import br.com.ervbtech.ftc.data.user.User;
import br.com.ervbtech.ftc.ui.components.Item;

@Entity(tableName = "financial_track")
public class FinancialTrack {

    @NonNull
    @PrimaryKey
    public String uuid;

    @ColumnInfo(name = "date")
    public Date date;

    @ColumnInfo(name = "value")
    public Double value;

    @ColumnInfo(name = "type")
    public int typeFinance;

    @Embedded(prefix = "user_")
    public User user;

    @Embedded(prefix = "category_")
    public Category category;

    @Ignore
    public CategoryEnum type;
}
