package br.com.ervbtech.ftc.data.user;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class User {

    @NonNull
    @PrimaryKey
    public String uuid;

    @ColumnInfo(name = "phone")
    public String phone;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "password")
    public String password;

    public User(String uuid, String phone, String name, String password) {
        this.uuid = uuid;
        this.phone = phone;
        this.name = name;
        this.password = password;
    }
}
