package br.com.ervbtech.ftc.data.financialtrack;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.Date;
import java.util.List;

@Dao
public interface FinancialTrackDao {

    @Query("SELECT * FROM financial_track ORDER BY date DESC")
    LiveData<List<FinancialTrack>> getAll();

    @Query("SELECT * FROM financial_track WHERE uuid IN (:ids)")
    LiveData<List<FinancialTrack>> loadAllByIds(String[] ids);

    @Query("SELECT * FROM financial_track WHERE date = :date LIMIT 1")
    FinancialTrack findByData(Date date);

    @Insert
    void insertAll(FinancialTrack... financialTracks);

    @Delete
    void delete(FinancialTrack financialTrack);
}
