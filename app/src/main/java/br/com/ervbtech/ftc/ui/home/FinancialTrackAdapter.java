package br.com.ervbtech.ftc.ui.home;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.ViewStubCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.ervbtech.ftc.R;
import br.com.ervbtech.ftc.data.financialtrack.FinancialTrack;
import br.com.ervbtech.ftc.data.financialtrack.TypeFinanceEnum;

public class FinancialTrackAdapter extends RecyclerView.Adapter<FinancialTrackAdapter.MyViewHolder> {
    private List<FinancialTrack> mDataset;
    private Activity mActivity;
    private FinancialTrack mRecentlyDeletedItem;
    private int mRecentlyDeletedItemPosition;
    private OnDeleteListener deleteListener;
    private OnUndoDeleteListener undoDeleteListener;

    public FinancialTrackAdapter(Activity mActivity) {
        this.mDataset = new ArrayList<>();
        this.mActivity = mActivity;

    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RelativeLayout layout;
        public MyViewHolder(RelativeLayout v) {
            super(v);
            layout = v;
        }
    }

    public static interface OnDeleteListener {
        void onDelete(FinancialTrack track);
    }

    public static interface OnUndoDeleteListener {
        void onUndoDelete(FinancialTrack track);
    }

    public Context getContext() {
        return mActivity.getBaseContext();
    }

    public void setDeleteListener(OnDeleteListener deleteListener) {
        this.deleteListener = deleteListener;
    }

    public void setUndoDeleteListener(OnUndoDeleteListener undoDeleteListener) {
        this.undoDeleteListener = undoDeleteListener;
    }

    public void setDataset(List<FinancialTrack> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        mRecentlyDeletedItem = mDataset.get(position);
        mRecentlyDeletedItemPosition = position;
        showUndoSnackbar();
        if (deleteListener != null) {
            deleteListener.onDelete(mRecentlyDeletedItem);
        }
    }

    private void showUndoSnackbar() {
        View view = mActivity.findViewById(R.id.container);
        Snackbar snackbar = Snackbar.make(view, R.string.snack_bar_text,
                Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.snack_bar_undo, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                undoDelete();
            }
        });
        snackbar.show();
    }

    private void undoDelete() {
        if (undoDeleteListener != null) {
            undoDeleteListener.onUndoDelete(mRecentlyDeletedItem);
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FinancialTrackAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_financial_track, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final FinancialTrack item = mDataset.get(position);
        String description = holder.itemView.getContext().getResources().getString(item.type.getResourceDescription());
        String detail = new SimpleDateFormat("dd MMM yyyy").format(item.date);
        ((ImageView)holder.layout.findViewById(R.id.list_item_icon)).setImageResource(item.type.getResourceIcon());
        ((TextView)holder.layout.findViewById(R.id.ftDescription)).setText(description);
        ((TextView)holder.layout.findViewById(R.id.ftDetail)).setText(detail);
        ((TextView)holder.layout.findViewById(R.id.tvValue)).setText(NumberFormat.getCurrencyInstance().format(item.value));
        ((TextView)holder.layout.findViewById(R.id.ftTypeFinance)).setText(holder.itemView.getContext().getResources().getString(TypeFinanceEnum.of(item.typeFinance).getResourceString()));

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}