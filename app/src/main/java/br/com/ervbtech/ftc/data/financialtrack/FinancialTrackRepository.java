package br.com.ervbtech.ftc.data.financialtrack;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import br.com.ervbtech.ftc.AddActivity;
import br.com.ervbtech.ftc.data.AppDatabase;
import br.com.ervbtech.ftc.data.category.CategoryEnum;

public class FinancialTrackRepository {

    private FinancialTrackDao dao;
    private LiveData<List<FinancialTrack>> allTracks;

    public FinancialTrackRepository(Application application) {
        dao = AppDatabase.getInstance().financialTrackDao();
        allTracks = dao.getAll();
    }

    public void insert(FinancialTrack track) {
        new InsertFinancialTrackAsyncTask(dao).execute(track);
    }

    public void delete(FinancialTrack track) {
        new DeleteFinancialTrackAsyncTask(dao).execute(track);
    }

    public LiveData<List<FinancialTrack>> getAllTracks() {
        return allTracks;
    }

    private static class InsertFinancialTrackAsyncTask extends AsyncTask<FinancialTrack, Void, Void> {

        private FinancialTrackDao dao;

        private InsertFinancialTrackAsyncTask(FinancialTrackDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(FinancialTrack... financialTracks) {
            final FinancialTrack track = financialTracks[0];
            final AppDatabase database = AppDatabase.getInstance();
            if (track.uuid == null) {
                track.uuid = UUID.randomUUID().toString();
            }
            if (track.date == null) {
                track.date = new Date();
            }
            if (track.category == null) {
                track.category = database.categoryDao().findById(track.type.getId());
            }
            if (track.user == null) {
                track.user = database.userDao().findFirst();
            }
            this.dao.insertAll(track);
            return null;
        }
    }

    private static class DeleteFinancialTrackAsyncTask extends AsyncTask<FinancialTrack, Void, Void> {

        private FinancialTrackDao dao;

        private DeleteFinancialTrackAsyncTask(FinancialTrackDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(FinancialTrack... financialTracks) {
            this.dao.delete(financialTracks[0]);
            return null;
        }
    }
}
