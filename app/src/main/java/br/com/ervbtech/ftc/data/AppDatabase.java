package br.com.ervbtech.ftc.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.Executors;

import br.com.ervbtech.ftc.data.category.Category;
import br.com.ervbtech.ftc.data.category.CategoryDao;
import br.com.ervbtech.ftc.data.category.CategoryData;
import br.com.ervbtech.ftc.data.financialtrack.FinancialTrack;
import br.com.ervbtech.ftc.data.financialtrack.FinancialTrackDao;
import br.com.ervbtech.ftc.data.user.User;
import br.com.ervbtech.ftc.data.user.UserDao;
import br.com.ervbtech.ftc.data.user.UserData;
import br.com.ervbtech.ftc.utils.converters.Converters;

@Database(entities = { User.class, Category.class, FinancialTrack.class }, version = 1)
@TypeConverters({ Converters.class })
public abstract class AppDatabase extends RoomDatabase {
    private static final String DB_NAME = "ftc.db";
    private static AppDatabase instance;
    public abstract UserDao userDao();
    public abstract CategoryDao categoryDao();
    public abstract FinancialTrackDao financialTrackDao();

    public static synchronized  AppDatabase getInstance() {
        return instance;
    }

    public static AppDatabase setInstance(Context context) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DB_NAME)
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Executors.newSingleThreadExecutor().execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            getInstance().categoryDao().insertAll(CategoryData.populateCategoriesData());
                                            getInstance().userDao().insertAll(UserData.populateUsersData());
                                        }
                                    });
                                }
                            })
                            .build();
                }
            }
        }
        return instance;
    }
}