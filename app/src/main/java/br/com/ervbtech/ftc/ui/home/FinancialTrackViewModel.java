package br.com.ervbtech.ftc.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import br.com.ervbtech.ftc.data.financialtrack.FinancialTrackRepository;
import br.com.ervbtech.ftc.data.financialtrack.FinancialTrack;

public class FinancialTrackViewModel extends AndroidViewModel {

    private FinancialTrackRepository repository;
    private LiveData<List<FinancialTrack>> tracks;

    public FinancialTrackViewModel(@NonNull Application application) {
        super(application);
        repository = new FinancialTrackRepository(application);
        tracks = repository.getAllTracks();
    }

    public void insert(FinancialTrack track) {
        repository.insert(track);
    }

    public void delete(FinancialTrack track) {
        repository.delete(track);
    }

    public LiveData<List<FinancialTrack>> getTracks() {
        return tracks;
    }
}
