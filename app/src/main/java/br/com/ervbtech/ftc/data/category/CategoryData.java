package br.com.ervbtech.ftc.data.category;

public class CategoryData {

    public static Category[] populateCategoriesData() {
        return new Category[] {
                new Category(1, "Combustível"),
                new Category(2, "Contrato"),
                new Category(3, "Frete"),
                new Category(4, "Alimentação"),
                new Category(5, "Bonificação"),
                new Category(6, "Mecânica"),
                new Category(7, "Peças"),
                new Category(8, "Escritório"),
                new Category(9, "Seguro"),
                new Category(10, "Limpeza"),
                new Category(11, "Telefonia"),
                new Category(12, "Saúde")
        };
    }

}
