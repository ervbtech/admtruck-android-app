package br.com.ervbtech.ftc.data.category;

import java.util.HashMap;
import java.util.Map;

import br.com.ervbtech.ftc.R;

public enum CategoryEnum {

    FUEL(1, R.string.fuel, R.drawable.ic_baseline_local_gas_station_24),
    CONTRACT(2,  R.string.contract, R.drawable.ic_baseline_assignment_24),
    FREIGHT(3, R.string.freight, R.drawable.ic_local_shipping_24),
    FOOD(4, R.string.food, R.drawable.ic_baseline_restaurant_24),
    BONUS(5, R.string.bonus, R.drawable.ic_baseline_card_giftcard_24),
    MECHANICAL(6, R.string.mechanical, R.drawable.ic_baseline_build_24),
    PARTS(7, R.string.parts, R.drawable.ic_miscellaneous_services_24),
    OFFICE(8, R.string.office, R.drawable.ic_local_printshop_24),
    INSURANCE(9, R.string.insurance, R.drawable.ic_baseline_security_24),
    CLEANING(10, R.string.cleaning, R.drawable.ic_baseline_local_car_wash_24),
    TELEPHONY(11, R.string.telephony, R.drawable.ic_baseline_call_24),
    HEALTH(12, R.string.health, R.drawable.ic_medical_services_24);

    private int id;
    private int resourceDescription;
    private int resourceIcon;

    private CategoryEnum(int id, int resourceDescription, int resourceIcon) {
        this.id = id;
        this.resourceDescription = resourceDescription;
        this.resourceIcon = resourceIcon;
    }

    public int getId() {
        return id;
    }

    public int getResourceDescription() {
        return resourceDescription;
    }

    public int getResourceIcon() {
        return resourceIcon;
    }

    private static Map<Integer, CategoryEnum> map = new HashMap<>();

    static {
        for (CategoryEnum enm : values()) {
            map.put(enm.id, enm);
        }
    }

    public static CategoryEnum of(int id) {
        return map.get(id);
    }

}
