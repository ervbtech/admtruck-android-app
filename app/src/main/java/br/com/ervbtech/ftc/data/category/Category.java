package br.com.ervbtech.ftc.data.category;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "category")
public class Category {

    @NonNull
    @PrimaryKey
    public Integer id;

    @ColumnInfo(name = "name")
    public String name;

    public Category(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

}
