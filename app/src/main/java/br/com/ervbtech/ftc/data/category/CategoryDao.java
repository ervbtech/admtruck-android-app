package br.com.ervbtech.ftc.data.category;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CategoryDao {

    @Query("SELECT * FROM category")
    List<Category> getAll();

    @Query("SELECT * FROM category WHERE id IN (:ids)")
    List<Category> loadAllByIds(Integer[] ids);

    @Query("SELECT * FROM category WHERE id = :id")
    Category findById(Integer id);

    @Query("SELECT * FROM category WHERE name LIKE :name LIMIT 1")
    Category findByName(String name);

    @Insert
    void insertAll(Category... categories);

    @Delete
    void delete(Category category);
}
