package br.com.ervbtech.ftc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.button.MaterialButtonToggleGroup;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

import br.com.ervbtech.ftc.data.category.CategoryEnum;
import br.com.ervbtech.ftc.ui.components.HorizontalCarouselRecyclerView;
import br.com.ervbtech.ftc.ui.components.ItemAdapter;
import br.com.ervbtech.ftc.utils.watchers.MoneyTextWatcher;

public class AddActivity extends AppCompatActivity {


    public static final String EXTRA_CATEGORY_ID = "EXTRA_CATEGORY_ID";
    public static final String EXTRA_TYPE_FINANCE = "EXTRA_TYPE_FINANCE";
    public static final String EXTRA_VALUE = "EXTRA_VALUE";

    private HorizontalCarouselRecyclerView recyclerView;
    private ItemAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ImageView imageView = (ImageView) findViewById(R.id.image_header);
        Glide.with(this).load(R.drawable.header_add).into(imageView);

        recyclerView = (HorizontalCarouselRecyclerView) findViewById(R.id.horizontal_carousel);
        recyclerView.setHasFixedSize(true);

        final List<CategoryEnum> dataSet = Arrays.asList(CategoryEnum.values());

        mAdapter = new ItemAdapter();
        recyclerView.initialize(mAdapter);
        recyclerView.setViewsToChangeColor(Arrays.asList(R.id.list_item_background, R.id.list_item_text));
        mAdapter.setItems(dataSet);

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        findViewById(R.id.progress).setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                },
                1800);

        final EditText edValue = findViewById(R.id.etValue);
        edValue.addTextChangedListener(new MoneyTextWatcher(edValue));

        final MaterialButtonToggleGroup toggleButton = findViewById(R.id.toggleButton);
        final MaterialButton back = findViewById(R.id.back_button);
        final MaterialButton save = findViewById(R.id.save_button);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean formValid = true;
                        if (edValue.getText() != null && edValue.getText().toString().isEmpty()) {
                            formValid &= false;
                        }
                        if (mAdapter.getSelected() == null) {
                            formValid &= false;
                        }

                        if (formValid) {
                            try {
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra(EXTRA_CATEGORY_ID, mAdapter.getSelected().getId());
                                returnIntent.putExtra(EXTRA_VALUE, NumberFormat.getCurrencyInstance().parse(edValue.getText().toString()).doubleValue());
                                returnIntent.putExtra(EXTRA_TYPE_FINANCE, toggleButton.getCheckedButtonId() == R.id.income ? 1 : 2);
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } catch (ParseException e) {
                                showMessage(R.string.message_generic_error);
                            }
                        } else {
                            showMessage(R.string.required_field_message);
                        }
                    }
                });
            }
        });

    }

    private void showMessage(int messageResource) {
        Toast.makeText(this, messageResource, Toast.LENGTH_SHORT).show();
    }
}
