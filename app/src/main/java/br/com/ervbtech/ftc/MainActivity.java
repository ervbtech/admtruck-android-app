package br.com.ervbtech.ftc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

import br.com.ervbtech.ftc.data.AppDatabase;
import br.com.ervbtech.ftc.data.category.CategoryEnum;
import br.com.ervbtech.ftc.data.financialtrack.FinancialTrack;
import br.com.ervbtech.ftc.ui.components.SwipeToDeleteCallback;
import br.com.ervbtech.ftc.ui.home.FinancialTrackAdapter;
import br.com.ervbtech.ftc.ui.home.FinancialTrackViewModel;
import br.com.ervbtech.ftc.ui.listeners.AppBarStateChangeListener;

public class MainActivity extends AppCompatActivity {

    public static final int ADD_FINANCIAL_TRACK_REQUEST = 1;

    private RecyclerView recyclerView;
    private FinancialTrackAdapter mAdapter;
    private FinancialTrackViewModel financialTrackViewModel;
    private RecyclerView.LayoutManager layoutManager;
    private View emptyInfo;
    private View nestedScrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppDatabase.setInstance(this);


        final FloatingActionButton fabAdd = findViewById(R.id.floating_action_button);
        final FloatingActionButton fabAddBottom = findViewById(R.id.floating_action_button_bottom);
        ImageView imageView = (ImageView) findViewById(R.id.image_header);
        Toolbar toolbar = findViewById(R.id.toolbar);
        emptyInfo = findViewById(R.id.empty_info);
        nestedScrollView = findViewById(R.id.nested_scroll_view);
        recyclerView = (RecyclerView) findViewById(R.id.financial_track_recycler_view);

        Glide.with(this).load(R.drawable.media).into(imageView);
        final CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout);
        ((AppBarLayout)findViewById(R.id.appBarLayout)).addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                if (State.COLLAPSED.equals(state)) {
                    fabAddBottom.show();
                } else {
                    fabAddBottom.hide();
                }
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new FinancialTrackAdapter(this);
        mAdapter.setDeleteListener(new FinancialTrackAdapter.OnDeleteListener() {
            @Override
            public void onDelete(FinancialTrack track) {
                financialTrackViewModel.delete(track);
            }
        });
        mAdapter.setUndoDeleteListener(new FinancialTrackAdapter.OnUndoDeleteListener() {
            @Override
            public void onUndoDelete(FinancialTrack track) {
                financialTrackViewModel.insert(track);
            }
        });
        recyclerView.setAdapter(mAdapter);

        financialTrackViewModel = new ViewModelProvider(this).get(FinancialTrackViewModel.class);
        financialTrackViewModel.getTracks().observe(this, new Observer<List<FinancialTrack>>() {
            @Override
            public void onChanged(List<FinancialTrack> tracks) {
                BigDecimal totalBudget = BigDecimal.ZERO;
                if (tracks != null && tracks.size() > 0) {
                    emptyInfo.setVisibility(View.GONE);
                    nestedScrollView.setVisibility(View.VISIBLE);
                    for (FinancialTrack item : tracks) {
                        item.type = CategoryEnum.of(item.category.id);
                        if (item.typeFinance == 1) {
                            totalBudget = totalBudget.add(BigDecimal.valueOf(item.value)).setScale(9, BigDecimal.ROUND_HALF_EVEN);
                        } else if (item.typeFinance == 2) {
                            totalBudget = totalBudget.subtract(BigDecimal.valueOf(item.value)).setScale(9, BigDecimal.ROUND_HALF_EVEN);
                        }
                    }
                    mAdapter.setDataset(tracks);
                } else {
                    emptyInfo.setVisibility(View.VISIBLE);
                    nestedScrollView.setVisibility(View.GONE);
                }
                toolbarLayout.setTitle(NumberFormat.getCurrencyInstance().format(totalBudget));
            }
        });
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(mAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        fabAdd.setOnClickListener(addClickListener());
        fabAddBottom.setOnClickListener(addClickListener());
    }

    @NotNull
    private View.OnClickListener addClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent insertIntent = new Intent(MainActivity.this, AddActivity.class);

                if (insertIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
                    MainActivity.this.startActivityForResult(insertIntent, ADD_FINANCIAL_TRACK_REQUEST);
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_FINANCIAL_TRACK_REQUEST) {
            if (Activity.RESULT_OK == resultCode) {
                FinancialTrack track = new FinancialTrack();
                track.type = CategoryEnum.of(data.getIntExtra(AddActivity.EXTRA_CATEGORY_ID, -1));
                track.typeFinance = data.getIntExtra(AddActivity.EXTRA_TYPE_FINANCE, 1);
                track.value = data.getDoubleExtra(AddActivity.EXTRA_VALUE, 0.0);
                financialTrackViewModel.insert(track);
                Toast.makeText(this, R.string.success_message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}