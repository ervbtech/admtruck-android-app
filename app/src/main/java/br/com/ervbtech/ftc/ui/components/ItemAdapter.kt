package br.com.ervbtech.ftc.ui.components

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.ervbtech.ftc.R
import br.com.ervbtech.ftc.data.category.CategoryEnum

class ItemAdapter : RecyclerView.Adapter<ItemViewHolder>() {

    private var items: List<CategoryEnum> = listOf()
    private var selected: CategoryEnum? = null
    private var selectedPosition: Int? = null
    private var selectionChange: View.OnClickListener? = null;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
            ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_carousel, parent, false))



    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.tag = position
        holder.itemView.setOnClickListener {
            selected = items[position];
            selectedPosition = position;
            holder.selected();
            selectionChange?.onClick(holder.itemView)
        }
    }

    override fun getItemCount() = items.size

    fun getSelected() = selected

    fun getSelectedPosition() = selectedPosition

    fun setSelectionChangeListener(listener: View.OnClickListener) {
        selectionChange = listener
    }

    fun setItems(newItems: List<CategoryEnum>) {
        items = newItems
        notifyDataSetChanged()
    }


}

class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(item: CategoryEnum) {
        view.findViewById<TextView>(R.id.list_item_text).text = view.resources.getString(item.resourceDescription);
        view.findViewById<ImageView>(R.id.list_item_icon).setImageResource(item.resourceIcon)
    }
    fun selected() {
        view.findViewById<ImageView>(R.id.list_item_background).setColorFilter(view.context.resources.getColor(R.color.secondaryColor));
        view.findViewById<TextView>(R.id.list_item_text).setTextColor(view.context.resources.getColor(R.color.secondaryColor));
    }

}