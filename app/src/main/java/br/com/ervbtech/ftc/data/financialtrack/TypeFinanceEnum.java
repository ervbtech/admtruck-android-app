package br.com.ervbtech.ftc.data.financialtrack;

import java.util.HashMap;
import java.util.Map;

import br.com.ervbtech.ftc.R;

public enum TypeFinanceEnum {

    INCOME(1, R.string.income),
    EXPENSE(2, R.string.expense);

    private int id;
    private int resourceString;

    private TypeFinanceEnum(int id, int resourceString) {
        this.id = id;
        this.resourceString = resourceString;
    }

    public int getId() {
        return id;
    }

    public int getResourceString() {
        return resourceString;
    }

    private static Map<Integer, TypeFinanceEnum> map = new HashMap<>();

    static {
        map.put(INCOME.id, INCOME);
        map.put(EXPENSE.id, EXPENSE);
    }

    public static TypeFinanceEnum of(int id) {
        return map.get(id);
    }

}
